package br.com.wvieira;

import javax.mail.MessagingException;
import java.io.IOException;
import java.security.GeneralSecurityException;

public class TesteEnvioGmail {
    public static void main(String[] args)  throws IOException, GeneralSecurityException, MessagingException {

        GmailAPI mail = new GmailAPI();
        mail.sendMessage("/gmailcredentials/credentials.json", "tokens/<EMAIL_FROM>","<EMAIL_TO>","<EMAIL_FROM>",
                "Teste # Gmail API","Texto do corpo do email");
    }
}
